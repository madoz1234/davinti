<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_task', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tgl', 255)->nullable();
            $table->integer('status')->default(0)->comment('0:Aktif,1:Tidak Aktif');
            $table->text('tugas');
            $table->text('keterangan')->nullable();
            $table->text('lat');
            $table->text('lng');
            $table->integer('user_id')->unsigned()->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::create('log_trans_task', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('tgl', 255)->nullable();
            $table->integer('status')->default(0)->comment('0:Aktif,1:Tidak Aktif');
            $table->text('tugas');
            $table->text('keterangan')->nullable();
            $table->text('lat');
            $table->text('lng');
            $table->integer('user_id')->unsigned()->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_task');
        Schema::dropIfExists('trans_task');
    }
}
