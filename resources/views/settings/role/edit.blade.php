<form action="{{ route($routes.'.saveData', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Role</h5>
    </div>
    <div class="modal-body">
        {{-- <p class="text-muted">Please fill the information to continue</p> --}}
        <div class="form-group">
            <label class="control-label">Role</label>
            <input type="text" name="name" class="form-control" placeholder="Role" value="{{ $record->name }}" required>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>