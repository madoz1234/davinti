@extends('layouts.list')

@section('title', 'Assign Permissions')

@section('side-header')
    <div class="btn-group" style="left: -15px;">
        <a href="{{ route('setting.users.index') }}" type="button" class="btn btn-grey ada" style="border-radius: 20px;">User</a>
        <a href="{{ route('setting.roles.index') }}" type="button" class="btn btn-grey ada active" style="border-radius: 20px;">Role</a>
    </div>
@endsection

@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-name">Role</label>
        <input type="text" class="form-control filter-control" name="filter[name]" data-post="name" placeholder="Role">
    </div>
@endsection
