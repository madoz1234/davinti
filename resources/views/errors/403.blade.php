@extends('errors::minimal')

@section('title', __('Forbidden'))
@section('code', '403')
@section('message', __($exception->getMessage() ?: 'Forbidden'))

@section('button', '<div class="">
                      <div class="codes">
                         <a class="btn btn-cancel back" href="'.route('idproo.login').'">Kembali</a>
                      </div>
                    </div>')
