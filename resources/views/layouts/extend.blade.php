<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name') }}</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	{{-- <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon.ico') }}" /> --}}
	{{-- <link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}" /> --}}

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="{{ asset('libs/assets/font-awesome/css/font-awesome.min.css') }}" type="text/css" />

	<!-- CSS Files -->
	<link href="{{ asset('ext/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('ext/assets/css/material-bootstrap-wizard.css') }}" rel="stylesheet" />

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="{{ asset('ext/assets/css/demo.css') }}" rel="stylesheet" /></head>

<body>
  @yield('content')
</body>
  
  <script src="{{ asset('ext/assets/js/jquery-2.2.4.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('ext/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('ext/assets/js/jquery.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
  <script src="{{ asset('libs/jquery/redirect/jquery.redirect.js') }}"></script>

	<!--  Plugin for the Wizard -->
	<script src="{{ asset('ext/assets/js/material-bootstrap-wizard.js') }}"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
  <script src="{{ asset('ext/assets/js/jquery.validate.min.js') }}"></script>
  <script type="text/javascript">
    $(document).on('click', '.logout', function(e){
      $.redirect("{{ route('logout') }}", { _token: "{{ csrf_token() }}" })
    });
    $(document).on('click', '.save-data', function(e){
			$("#saveData").ajaxSubmit({
        success: function(resp){
          $('.show-sukses').css('display','');
          $('.form-input').css('display','none');
        },
        error: function(resp){
          $('.showerror').fadeIn('slideUp', function(e) {});
          time = 5;
				  interval = setInterval(function(){
            time--;
            if(time == 0){
              clearInterval(interval);
              $('.showerror').fadeOut('slideUp', function(e) {
              });
            }
          },1000)
        }
      });
		});
    $('.kembali').click(function() {
        location.reload();
    });
    
  </script>
</html>
