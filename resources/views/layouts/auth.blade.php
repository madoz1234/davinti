<!DOCTYPE html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>{{ config('app.name') }}</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  {{-- <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.ico') }}"/> --}}
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="{{ asset('src/css/font.css') }}">
  <link rel="stylesheet" href="{{ asset('src/css/login/app.css') }}">
  <link rel="stylesheet" href="{{ asset('gate/css/login.css') }}">

  <meta name="theme-color" content="#fafafa">
</head>

<body>

  <div class="w-100 h-100">
    <div class="row mx-0 h-100 align-items-center">

      <div class="d-none d-lg-flex col-lg-9 col-sm-9 px-0">
        <div class="w-75 px-3 ml-auto mr-5 text-right">
        	<h2 class="text-white">
           Davinti Group
         </h2>
         {{-- <img src="{{ asset('src/img/logo-edited.png') }}" class="heading-img" alt="Main Icon" height="150vh"> --}}
         <hr class="my-4">
         <h3 class="text-white">
          -Description Example
        </h3>
      </div>

      <div class="footer px-5 py-3 text-white w-50 text-right" style="left: 8rem;">
        Copyright &copy; {!! $mytime = Carbon\Carbon::now()->format('Y'); !!} Davinti Group. All rights reserved. <a href="#">Terms of Use</a> · <a href="#">Privacy policy</a>
      </div>
    </div>

    <div class="col-sm-3 px-0 h-100 bg-white d-flex">
      @yield('content')
    </div>
  </div>
</div>

<script src="src/js/jquery-3.3.1.slim.min.js"></script>
<script src="src/js/popper.min.js"></script>
</body>

</html>
