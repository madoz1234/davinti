<header id="header" class="app-header navbar" role="menu">
    <!-- navbar header -->
    <div class="navbar-header" style="background-color: #0f2233; color: #ffffff;">
      <button class="pull-right visible-xs" ui-toggle-class="show" target=".navbar-collapse">
        <i class="glyphicon glyphicon-cog"></i>
      </button>
      <button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
        <i class="glyphicon glyphicon-align-justify"></i>
      </button>
      <!-- brand -->
      <a href="#/" class="navbar-brand text-lt" style="margin-bottom: -0.5px;">
        <!-- <i class="fa fa-btc" class="hide"></i> -->
        {{-- <img src="{{ asset('src/img/logo-mini.png') }}">  --}}
        <span class="hidden-folded m-l-sm" style="font-size: 10px; font-weight: bold;">Davinti</span>
      </a>
      <!-- / brand -->
    </div>
    <!-- / navbar header -->

    <div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
      <!-- buttons -->
      <div class="nav navbar-nav hidden-xs">
        <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="app-aside-folded" target=".app" style="padding-left: 0px;padding-right: 0px;">
          <i class="fa fa-dedent fa-fw text" style="color: #ffffff;"></i>
          <i class="fa fa-indent fa-fw text-active" style="color: #ffffff;"></i>
        </a>
      </div>
      <!-- / buttons -->

    <!-- search form -->

    <!-- / search form -->

    <!-- nabar right -->
    @php 
    	$user = auth()->user();
    @endphp
    <ul class="nav navbar-nav navbar-right">
      <!-- dropdown -->
      <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
          <i class="icon-bell fa-fw" style="color: #ffffff;"></i>
          <span class="visible-xs-inline">Notifications</span>
          <span class="badge badge-sm up bg-danger pull-right-xs"></span>
        </a>
        <!-- dropdown -->
        <div class="dropdown-menu w-xl faster animated fadeInUp" style="overflow-y:scroll;overflow-x: hidden;max-height: 300px;" id="style-7">
          <div class="panel bg-white">
            <div class="panel-heading b-light bg-light">
              <strong>You have <span>0</span> notifications</strong>
            </div>
            <div class="list-group">
            </div>
           {{--  <div class="panel-footer text-sm">
              <a href="" class="pull-right"><i class="fa fa-cog"></i></a>
              <a href="#notes" data-toggle="class:show faster animated fadeInRight">See all the notifications</a>
            </div> --}}
          </div>
        </div>
        <!-- / dropdown -->
      </li>
      <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle clear" aria-expanded="false">
          <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
            <img src="{{ asset('src/img/a0.jpg') }}" alt="...">
            <i class="{{ isConnected() ? 'on' : 'off' }} md b-white bottom"></i>
          </span>
          <span class="hidden-sm hidden-md" style="color: #ffffff;">{{ auth()->user()->name }}</span> <b class="caret"></b>
        </a>
        <!-- dropdown -->
        <ul class="dropdown-menu faster animated fadeInRight w">
          <li>
            <a ui-sref="app.page.profile">Profile</a>
          </li>
          <li class="divider"></li>
          <li>
            <a href="javascript:void(0)" class="logout">Logout</a>
          </li>
        </ul>
        <!-- / dropdown -->
      </li>
    </ul>
    <!-- / navbar right -->
  </div>
</header>