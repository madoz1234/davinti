<script type="text/javascript">
	$(document).ready(function() {
		app.init();
var options = {
	chart: {
		height: 370,
		type: 'pie',
		events: {
			click: function (event, chartContext, config) {
				console.log("click");
			},
			dataPointSelection: function (event, chartContext, config) {
				console.log("dataPointSelection");
			}
		}
	},
	series: [{{$data}}],
	labels: ['Aktif', 'Tidak Aktif'],
	colors: ['#00dd1a', '#dd8600'],
}

	var chart = new ApexCharts(
	document.querySelector("#chart-employe"),
	options);
		chart.render();
	});
</script>
<div id="chart-employe"></div>
