<?php

namespace App\Http\Requests\Task;

use App\Http\Requests\FormRequest;

class TaskRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   	public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		$return = [
            'tgl'   		=> 'required',
            'tugas'   		=> 'required',
            'user_id'   	=> 'required',
            'lat' 			=> ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],             
            'lng' 			=> ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'tgl.required'      	=> 'Tanggal tidak boleh kosong',
        	'tugas.required'      	=> 'Tugas tidak boleh kosong',
        	'user_id.required'      => 'Employe tidak boleh kosong',
        	'lat.required'      	=> 'Latitude tidak boleh kosong',
        	'lat.regex'      		=> 'Inputkan Latitude dengan format yang benar',
        	'lng.required'      	=> 'Longitude tidak boleh kosong',
        	'lng.regex'      		=> 'Inputkan Longitude dengan format yang benar',
       ];
    }
}
