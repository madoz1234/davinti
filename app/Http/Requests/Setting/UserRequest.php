<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\FormRequest;

class UserRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   	public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		$return = [
            'nip'   		=> 'required|max:12|unique:sys_users,nip,'.request()->id,
            'name'   		=> 'required|max:255',
            'email'   		=> 'required|max:255|email|unique:sys_users,email,'.request()->id,
            'role'   		=> 'required',
            'jk'   			=> 'required',
            'tgl_lahir'   	=> 'required',
            'password'   	=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'name.required'      	=> 'Nama tidak boleh kosong',
        	'name.max'       	  	=> 'Maks Karakter (255)',
        	'nip.required'      	=> 'NIP tidak boleh kosong',
        	'nip.max'       	  	=> 'Maks Karakter (12)',
        	'nip.unique'       	  	=> 'NIP sudah ada',
        	'email.required'      	=> 'Email tidak boleh kosong',
        	'password.required'   	=> 'Password tidak boleh kosong',
        	'role.required'   		=> 'Role tidak boleh kosong',
        	'password.min'  		=> 'Min Karakter (6)',
        	'jk.required'  			=> 'Jenis Kelamin tidak boleh kosong',
        	'tgl_lahir.required'  	=> 'Tanggal Lahir tidak boleh kosong',
       ];
    }
}
