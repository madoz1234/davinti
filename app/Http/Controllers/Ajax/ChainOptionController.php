<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Auths\Role;
use App\Models\Attachments;
use App\Models\Files;
use App\Models\Task\Task;


use Datatables;

class ChainOptionController extends Controller
{
    
    function __construct()
    {
    }

    public function getPeta(Request $request)
    {
        $task = Task::find($request->id);
        $array[0] = $task->lat;
        $array[1] = $task->lng;
        return $array;
    }
}


