<?php

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/home', function(){
  return redirect()->route('dashboard.home.index');
});

Route::middleware('auth')->group(function() {
	Route::name('dashboard.')->prefix('dashboard')->namespace('Dashboard')->group( function() {
		Route::get('/', function(){
			return redirect()->route('dashboard.home.index');
		});
		Route::post('home/chartTask', 'HomeController@chartTask')->name('home.chartTask');
		Route::post('home/chartEmploye', 'HomeController@chartEmploye')->name('home.chartEmploye');
		Route::resource('home', 'HomeController');
	});

	Route::group(['prefix' => 'ajax/option', 'namespace' => 'Ajax'], function(){
	    Route::post('get-peta', 'ChainOptionController@getPeta');
	});

	Route::name('task.')->prefix('task')->namespace('Task')->group( function() {
	    Route::post('task/grid', 'TaskController@grid')->name('task.grid');
	    Route::get('task/{id}/detail','TaskController@detail')->name('task.detail');
		Route::resource('task', 'TaskController');
	});

	Route::name('setting.')->prefix('setting')->namespace('Setting')->group( function() {
		Route::get('/', function(){
			return redirect()->route('setting.users.index');
		});

		Route::post('users/grid', 'UserController@grid')->name('users.grid');
		Route::get('users/{id}/detail','UserController@detail')->name('users.detail');
		Route::resource('users', 'UserController');

		Route::post('log/grid', 'LogController@grid')->name('log.grid');
		Route::resource('log', 'LogController');

		Route::post('roles/grid', 'RoleController@grid')->name('roles.grid');
		Route::patch('roles/saveData/{id}','RoleController@saveData')->name('roles.saveData');
		Route::resource('roles', 'RoleController');
	});
});

Auth::routes();
