<?php

use Illuminate\Http\Request;

Route::post('/login', 'AuthController@login');
Route::middleware(['auth:api'])->group(function () {
	Route::get('/me', 'AuthController@me');
	Route::get('refresh-token','AuthController@refresh');
	Route::post('/logout', 'AuthController@logout');
});
// Route::post('/test/{test}/take', 'TestController@take');
// Route::resource('/test', 'TestController')->only(['index', 'show']);
// Route::get('/exam/{exam}', 'TestController@exam');
// Route::post('/exam/{exam}/answer/{question}', 'TestController@answer');
// Route::post('/exam/{exam}/finish', 'TestController@finish');
